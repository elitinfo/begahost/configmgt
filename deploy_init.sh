#!/bin/bash

# prompt beállítások
wget -qO - https://gitlab.com/elitinfo/begahost/configmgt/-/raw/main/bash.bashrc >> /etc/bash.bashrc

# időzóna beállítások
timedatectl set-timezone Europe/Budapest

# UTF-8 locale beállítás és generálás
echo "LC_ALL=en_US.UTF-8" >> /etc/environment
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
locale-gen en_US.UTF-8
