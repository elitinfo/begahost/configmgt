#!bin/bash

TARGET_DIR=$1
TARGET_ENV=$2
TARGET_PRJ=$3
TARGET_IP=`hostname -i`

mkdir ~/$TARGET_DIR
cd ~/$TARGET_DIR

python3 -m venv $TARGET_ENV
source $TARGET_ENV/bin/activate
cd ~/$TARGET_DIR/$TARGET_ENV
pip install django
django-admin startproject $TARGET_PRJ .
python manage.py migrate

cd ~/$TARGET_DIR/$TARGET_ENV/$TARGET_PRJ
sed -i "s/ALLOWED_HOSTS = \[\]/ALLOWED_HOSTS = \['$TARGET_IP'\]/g" settings.py 

echo 
echo Django project environment prepared.
echo Execute the following commands interactively to finish:
echo
echo source ~/$TARGET_DIR/$TARGET_ENV/bin/activate
echo cd ~/$TARGET_DIR/$TARGET_ENV
echo python manage.py createsuperuser
echo python manage.py runserver $TARGET_IP:8000
echo
echo Then open the following URL from your browser:
echo http://$TARGET_IP:8000
echo
echo Django admin:
echo http://$TARGET_IP:8000/admin
