#!/bin/bash

apt update

apt install apache2
ufw allow in "Apache Full"

apt install mysql-server

apt install php libapache2-mod-php php-mysql
