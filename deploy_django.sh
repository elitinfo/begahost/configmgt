#/bin/bash
apt update
VERSION_PYTHON=`python3 -V`
apt -y install python3-django
apt -y install python3-pip
apt -y install python3-venv
apt -y install sqlite3
VERSION_DJANGO=`django-admin --version`
ufw allow 8000
echo
echo Python/Django successfully installed.
echo Python version: $VERSION_PYTHON
echo Django version: $VERSION_DJANGO
