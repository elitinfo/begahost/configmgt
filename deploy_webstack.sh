#/bin/bash
apt update

# Django webstack install
VERSION_PYTHON=`python3 -V`
apt -y install python3-django
apt -y install python3-pip
apt -y install python3-venv
apt -y install sqlite3
VERSION_DJANGO=`django-admin --version`
ufw allow 8000

# PHP webstack install
apt -y install apache2
apt -y install mysql-server
apt -y install php libapache2-mod-php php-mysql
apt -y install curl
ufw allow in "Apache Full"

# Default LAMP vhost setup
VHOST=`hostname -f`

useradd -m web
chsh -s /bin/bash web

mkdir -p /var/www/$VHOST

if [[ ! -f /var/www/$VHOST/index.html ]]
then
    echo $VHOST > /var/www/$VHOST/index.html
fi

if [[ ! -f /var/www/$VHOST/pi.php ]]
then
    echo "<?php phpinfo();" > /var/www/$VHOST/pi.php
fi

chown -R web.web /var/www/$VHOST

if [[ ! -f /etc/apache2/sites-available/$VHOST.conf ]]
then
    wget -qO - https://gitlab.com/elitinfo/begahost/configmgt/-/raw/main/vhost_http.conf > /etc/apache2/sites-available/$VHOST.conf
    sed -i "s/xxxvhostxxx/$VHOST/g" /etc/apache2/sites-available/$VHOST.conf
    a2ensite $VHOST
    a2dissite 000-default
    service apache2 reload
fi

## SSL

# Install acme.sh
wget -O -  https://get.acme.sh | sh
sleep 5

# acme.sh register
/root/.acme.sh/acme.sh --register-account -m ssl@begahost.com
sleep 5

# acme.sh issue SSL cert
/root/.acme.sh/acme.sh --issue -d $VHOST -w /var/www/$VHOST
sleep 5

# acme.sh install SSL cert
mkdir /etc/apache2/ssl
/root/.acme.sh/acme.sh --install-cert -d $VHOST --keypath /etc/apache2/ssl/$VHOST.key --fullchainpath /etc/apache2/ssl/$VHOST.pem
sleep 5

# Apache SSL settings
wget -qO - https://gitlab.com/elitinfo/begahost/configmgt/-/raw/main/ssl-params.conf > /etc/apache2/conf-available/ssl-params.conf
a2enmod ssl
a2enmod headers
a2enconf ssl-params
service apache2 reload
sleep 5

# Apache SSL vhost update
wget -qO - https://gitlab.com/elitinfo/begahost/configmgt/-/raw/main/vhost_https.conf > /etc/apache2/sites-available/$VHOST.conf
sed -i "s/xxxvhostxxx/$VHOST/g" /etc/apache2/sites-available/$VHOST.conf
service apache2 reload

# Summary
echo
echo Python/Django stack successfully installed.
echo Python version: $VERSION_PYTHON
echo Django version: $VERSION_DJANGO

echo
echo LAMP stack successfully installed.
echo Default vhost: http://$VHOST
echo PHPinfo page is available under https://$VHOST/pi.php
echo PHPadmin is available under https://$VHOST/phpmyadmin

echo
apache2 -v
echo
mysql -V
echo
php -v

echo
echo SSL certs installed for $VHOST by ZeroSSL CA.
echo

##phpinfo
##phpmyadmin
