
set_prompt () {
    Last_Command=$?
    Blue='\[\e[01;34m\]'
    White='\[\e[01;37m\]'
    Red='\[\e[01;31m\]'
    Green='\[\e[01;32m\]'
    Yellow='\[\e[01;33m\]'
    Cyan='\[\e[0;36m\]'
    Gray='\[\e[1;90m\]'
    Magenta='\[\e[1;95m\]'
    Reset='\[\e[00m\]'
    FancyX='\342\234\227'
    Checkmark='\342\234\223'

    PS1="$White"
    if [[ $Last_Command == 0 ]]; then
        PS1+="$Gray[ $Green"rc$Last_Command"$Gray ]-["
    else
        if [ "$Last_Command" -lt 10 ]; then
            PS1+="$Gray[ $Red"rc$Last_Command"$Gray ]-["
        else
            PS1+="$Gray[$Red"rc$Last_Command"$Gray]-["
        fi
    fi
    if [[ $EUID == 0 ]]; then
        PS1+="$Yellow\\u@\$(hostname -f)$Gray]-[$Magenta\\w$Gray]$Red \\\$$Reset "

    else
        PS1+="$Green\\u@\$(hostname -f)$Gray]-[$Magenta\\w$Gray]$Green \\\$$Reset "
    fi
}
PROMPT_COMMAND='set_prompt'
